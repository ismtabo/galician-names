# Galician names

Simple Node+Typescript project. Galician names is an awesome cli tool to generate (surprisingly) typical names from Galicia.

## Install

Using npm:

```bash
npm install -g galician-names
```

## Usage

Update database of names from API:

```bash
galician-names update
```

Generate a random Galician name:

```bash
galician-names generate
```
