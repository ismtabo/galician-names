import fs from 'fs'
import config from './config'

if (
  config.NODE_ENV === 'production' &&
  !fs.existsSync(config.LOCAL_CONFIG_PATH)
) {
  fs.mkdirSync(config.LOCAL_CONFIG_PATH)
  console.log(`Create local config path at ${config.LOCAL_CONFIG_PATH}`)
}
