export class EmptyResponseError extends Error {
  message =
    'Empty response from API. Please, check your internet connection or report the error to the developer'
}
