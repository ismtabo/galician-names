#!/usr/bin/env node

// tslint:disable-next-line: no-var-requires
import { JSDOM } from 'jsdom'
import low from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'
import fetch from 'node-fetch'
import path from 'path'
import { from, Observable, of, throwError } from 'rxjs'
import { concatMap, filter, flatMap, map, tap, toArray } from 'rxjs/operators'
import config from './config'
import { EmptyResponseError } from './errors'

const adapter = new FileSync(path.join(config.LOCAL_CONFIG_PATH, 'db.json'))
const db = low(adapter)

const emptyBuilder = () => {}

function updateNames(args: any) {
  const urls = [
    'https://www.guiainfantil.com/servicios/nombres/nombres_gallegos1.htm',
    'https://www.guiainfantil.com/servicios/nombres/nombres_gallegos2.htm',
    'https://www.guiainfantil.com/servicios/nombres/nombres_gallegos3.htm',
    'https://www.guiainfantil.com/servicios/nombres/nombres_de_galicia.htm',
  ]

  function processNames(source: Observable<string>): Observable<string[]> {
    return source.pipe(
      tap((html) => {
        ;(html == null || html === '') && throwError(new EmptyResponseError())
      }),
      map((html) => new JSDOM(html)),
      flatMap((dom) =>
        Array.from(dom.window.document.querySelectorAll('td:first-of-type'))
      ),
      map((el) => (el ? el.textContent || '' : '')),
      filter((name) => typeof name === 'string' && name !== ''),
      toArray()
    )
  }

  of(...urls)
    .pipe(
      concatMap((url) => from(fetch(url))),
      concatMap((resp) => from(resp.text())),
      processNames
    )
    .subscribe({
      next: (names) => {
        db.set('names', names).write()
      },
      error: (error) =>
        console.error(error instanceof Error ? error.message : error),
    })
}

function generateName(args: any) {
  const names: string[] = db.get('names').value()
  if (!Array.isArray(names)) {
    console.error(
      `Error - there is no local names. Try 'galician-names update' to retrieve names from api.`
    )
    process.exit(1)
  }
  if (names.length < 1) {
    console.error(
      `Local names is empty. No name is generated. Try 'galician-names update' to retrieve names from api.`
    )
    process.exit(0)
  }
  const { length } = names
  const randomIndex = Math.floor(Math.random() * length)
  console.log(names[randomIndex])
  process.exit(0)
}

const yargs = require('yargs')
  .usage('Usage: $0 <command>')
  .command('update', 'update local db', emptyBuilder, updateNames)
  .command('generate', 'generate random name', emptyBuilder, generateName)
  .demandCommand(1)
  .help('h')
  .alias('h', 'help')
  .version().argv
