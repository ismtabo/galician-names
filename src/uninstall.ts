import fs from 'fs'
import config from './config'

if (
  config.NODE_ENV === 'production' &&
  fs.existsSync(config.LOCAL_CONFIG_PATH)
) {
  fs.rmdirSync(config.LOCAL_CONFIG_PATH, { recursive: true })
  console.log(`Removing local config folder`)
}
