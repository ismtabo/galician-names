import getAppDataPath from 'appdata-path'
import dotenv from 'dotenv'
import path from 'path'

const result = dotenv.config()
const envs = result.error ? {} : result.parsed
const environments = {
  development: {
    LOCAL_CONFIG_PATH: '.',
  },
  production: {
    LOCAL_CONFIG_PATH: path.join(getAppDataPath(), 'galician-names'),
  },
}
export default {
  NODE_ENV: 'production',
  ...envs,
  ...(envs && envs.NODE_ENV === 'development'
    ? environments.development
    : environments.production),
}
